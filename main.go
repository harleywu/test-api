package main

import (
	"github.com/gin-gonic/gin"
	"github.com/shopee/test-api/config"
)

var router = gin.Default()

func main() {
	router.Run(config.CONFIG.Port)
}




