package config

import (
	"encoding/json"
	"io/ioutil"
)

// Configuration ...
type Configuration struct {
	Port       string `json:"port"`
	Postgresql struct {
		Host     string `json:"host"`
		Port     int    `json:"port"`
		User     string `json:"user"`
		Password string `json:"password"`
		Dbname   string `json:"dbname"`
	} `json:"postgresql"`
}

// CONFIG
var CONFIG Configuration

// LoadCONFIG
func LoadCONFIG() (err error) {
	b, err := ioutil.ReadFile("config/config.json")
	if err != nil {
		return err
	}

	if err := json.Unmarshal(b, &CONFIG); err != nil {
		return err
	}
	return nil
}

func init() {
	err := LoadCONFIG()
	if err != nil {
		panic(err)
	}
}
