package main

import (
	"time"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/shopee/test-api/handler/food"
)

func init(){
	router.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"PUT", "PATCH", "GET", "POST", "DELETE"},
		AllowHeaders:     []string{"Origin", "Authorization", "Content-Type", "Content-Lenght","X-Requested-With"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	v1 := router.Group("/v1/shopeetest")
	//ping
	v1.GET("/ping",func(context *gin.Context) {
		context.JSON(200,gin.H{"version":"Shoope test Active v1","status":"Active"})
	})
	v1.POST("",food.InserHandler)
	v1.GET("",food.ListHandler)

}
