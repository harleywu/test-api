/*
 Navicat Premium Data Transfer

 Source Server         : postgres-bamms
 Source Server Type    : PostgreSQL
 Source Server Version : 100005
 Source Host           : 18.136.200.232:5432
 Source Catalog        : noticeme1
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100005
 File Encoding         : 65001

 Date: 27/12/2018 00:28:57
*/


-- ----------------------------
-- Sequence structure for food_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."food_id_seq";
CREATE SEQUENCE "public"."food_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for food
-- ----------------------------
DROP TABLE IF EXISTS "public"."food";
CREATE TABLE "public"."food" (
  "id" int8 NOT NULL DEFAULT nextval('food_id_seq'::regclass),
  "data" jsonb,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;

-- ----------------------------
-- Records of food
-- ----------------------------
INSERT INTO "public"."food" VALUES (8, '{"name": "Test1", "price": 10000, "tax_code": 1}', '2018-12-26 17:03:36', NULL, NULL);
INSERT INTO "public"."food" VALUES (9, '{"name": "Test1", "price": 10000, "tax_code": 2}', '2018-12-26 17:04:09', NULL, NULL);
INSERT INTO "public"."food" VALUES (10, '{"name": "Test3", "price": 10000, "tax_code": 3}', '2018-12-26 17:04:16', NULL, NULL);
INSERT INTO "public"."food" VALUES (11, '{"name": "Lucky Stretch", "price": 1000, "tax_code": 2}', '2018-12-26 17:42:27', NULL, NULL);
INSERT INTO "public"."food" VALUES (12, '{"name": "Big Mac", "price": 1000, "tax_code": 1}', '2018-12-26 17:42:42', NULL, NULL);
INSERT INTO "public"."food" VALUES (13, '{"name": "Movie", "price": 150, "tax_code": 3}', '2018-12-26 17:42:58', NULL, NULL);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."food_id_seq"
OWNED BY "public"."food"."id";
SELECT setval('"public"."food_id_seq"', 14, true);

-- ----------------------------
-- Primary Key structure for table food
-- ----------------------------
ALTER TABLE "public"."food" ADD CONSTRAINT "food_pkey" PRIMARY KEY ("id");
