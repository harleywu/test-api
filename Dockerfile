#FROM golang:test-api

#ADD ./src /go/src/app
#WORKDIR /go/src/app
#ENV PORT=3001
#CMD ["go", "run", "main.go"]

#FROM golang:alpine
#RUN mkdir /app
#ADD . /app/
#WORKDIR /app
#RUN go build -o main .
#RUN adduser -S -D -H -h /app appuser
#USER appuser
#CMD ["./main"]

FROM golang:alpine
EXPOSE 2020
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN go get && go build -o main .
RUN adduser -S -D -H -h /app appuser
USER appuser
CMD ["./main"]
