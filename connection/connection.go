package connection

import (
	"github.com/jmoiron/sqlx"
	"log"
	_ "github.com/lib/pq"
	"fmt"
	"github.com/shopee/test-api/config"
)

var (
	pool *sqlx.DB
	err error
)

func init(){
	pgInfo := config.CONFIG.Postgresql
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		pgInfo.Host, pgInfo.Port, pgInfo.User, pgInfo.Password, pgInfo.Dbname)
	if pool, err = sqlx.Connect("postgres", psqlInfo); err != nil{
		log.Println("Connection Err : ", err.Error())
	} else{
		log.Println("connection Created Success")
	}
}

func GetConnection() (*sqlx.DB) {
	return pool
}
