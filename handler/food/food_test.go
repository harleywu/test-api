package food

import (
	"testing"
	"log"
	"encoding/json"
)

func TestListView(t *testing.T) {
	_,_,err:=listView()
	if err != nil {
		log.Fatal(err)
	}
}

func TestInsert(t *testing.T) {
	food := &Food_insert{Data:json.RawMessage(`{"name" : "boxer", "tax_code" : 3, "price" : 150}`)}
	err:=insert(food)
	if err != nil {
		t.Error(err)
	}
}

func TestCalculation(t *testing.T) {
	food_type_1 := &Food{Name: "test 1", Tax_Code: 1, Price: 1000}
	food_type_2 := &Food{Name: "test 1", Tax_Code: 2, Price: 1000}
	food_type_3 := &Food{Name: "test 1", Tax_Code: 3, Price: 1000}

	ok,message :=calculated(food_type_1)
	if !ok {
		t.Error(message)
		return
	}
	if food_type_1.Amount != 11000{
		t.Error("err calculation 1 ")
	}

	ok,message = calculated(food_type_2)
	if !ok {
		t.Error(message)
		return
	}
	if food_type_2.Amount != 10210{
		t.Error("err calculation 2 ")
	}
	ok,message = calculated(food_type_3)
	if !ok {
		t.Error(message)
		return
	}
	if food_type_3.Amount != 10099{
		t.Error("err calculation 3 ")
	}
}