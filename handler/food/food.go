package food

import (
	"github.com/shopee/test-api/connection"
	"sync"
)

var db = connection.GetConnection()

func insert(food *Food_insert) (error){
	err :=db.QueryRow(`INSERT INTO food (data,created_at) VALUES($1,$2) RETURNING id`,food.Data,food.Created_at).Scan(&food.Id)
	return err
}

func listView()(*[]Food,*Sum_calculation,error){
	food_list :=[]Food{}
	sum :=&Sum_calculation{}
	var wgCalculation sync.WaitGroup

	rows,err:=db.Queryx(`SELECT data->>'name' AS name ,(data->>'tax_code')::INTEGER AS tax_code,(data->>'price')::float as price FROM food`)
	if err !=nil{
		return &food_list,sum,err
	}

	for rows.Next(){
		var food Food
		if err :=rows.StructScan(&food); err !=nil{
			return &food_list,sum,err
		}
		wgCalculation.Add(1)
		go func() {
			defer wgCalculation.Done()
			calculated(&food)
			sum.Grand_total    +=food.Amount
			sum.Tax_subtotal   +=food.Tax
			sum.Price_subtotal +=food.Price
			food_list = append(food_list,food)
		}()
	}
	wgCalculation.Wait()
	return &food_list,sum,err
}


func calculated(food *Food)(ok bool,message string){
	if food.Tax_Code == TYPE_FOOD{
		food.Type = "Food & Beverage"
		food.Refundable="Yes"
		//10% of Price
		food.Tax=food.Price*10/100
	}else if food.Tax_Code == TYPE_TABACO{
		food.Type = "Tobacco"
		food.Refundable="No"
		//10 + (2% of Price )
		food.Tax=10+(food.Price*2/100)
	}else if food.Tax_Code == TYPE_ENTERTAINMENT{
		food.Type = "Entertainment"
		food.Refundable="No"
		if 0 < food.Price && food.Price < 100{
			//text free
			food.Tax=0
		}else if food.Price >= 100 {
			//1% of ( Price - 100)
			food.Tax=(food.Price-100)/100
		}
	}else {
		return false,"Your Type Is Wrong"
	}
	food.Amount=food.Tax + food.Price
	return true,"Ok"
}