package food

import (
	"time"
	"encoding/json"
)

const TYPE_FOOD           = 1
const TYPE_TABACO         = 2
const TYPE_ENTERTAINMENT  = 3

type Food struct {
	Id 			int64	  `json:"id,omitempty"`
	Name		string    `json:"name"`
	Tax_Code	int		  `json:"tax_code"`
	Type		string	  `json:"type"`
	Refundable  string	  `json:"refundable"`
	Price		float64	  `json:"price"`
	Tax         float64   `json:"tax"`
	Amount      float64	  `json:"amount"`
	Created_at  time.Time `json:"created_at,omitempty"`
	Updated_at  time.Time `json:"updated_at,omitempty"`
	Deleted_at  time.Time `json:"deleted_at,omitempty"`
}

type Food_insert struct {
	Id 			int64	  `json:"id"`
	Data json.RawMessage  `json:"data"`
	Created_at  time.Time `json:"created_at,omitempty"`
	Updated_at  time.Time `json:"updated_at,omitempty"`
	Deleted_at  time.Time `json:"deleted_at,omitempty"`
}

type Sum_calculation struct {
	Price_subtotal float64 `json:"price_subtotal"`
	Tax_subtotal   float64 `json:"tax_subtotal"`
	Grand_total    float64 `json:"grand_total"`
}

type Response struct {
	Status  string 		`json:"status"`
	Message string 		`json:"message"`
	Data    interface{}	`json:"data"`
}
