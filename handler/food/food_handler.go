package food

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func InserHandler(c *gin.Context){
	food :=Food_insert{}
	resp :=Response{Status:"400"}

	if err := c.BindJSON(&food); err != nil {
		resp.Message=err.Error()
		c.JSON(http.StatusBadRequest,resp )
		return
	}

	food.Created_at = time.Now()
	err :=insert(&food)
	if err !=nil{
		resp.Message=err.Error()
		c.JSON(http.StatusBadRequest,resp )
		return
	}

	resp.Status="200"
	resp.Message="Insert Success"
	resp.Data=&food
	c.JSON(http.StatusOK,resp)
}

func ListHandler(c *gin.Context){
	resp :=Response{Status:"400"}

	data,detail,err := listView()
	if err !=nil {
		resp.Message=err.Error()
		c.JSON(http.StatusBadRequest,resp )
		return
	}
	resp.Status="200"
	resp.Message="View Success"
	resp.Data=gin.H{
		"food  ":data,
		"detail":detail,
	}
	c.JSON(http.StatusOK,resp)
}
